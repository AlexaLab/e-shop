﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Электронный_магазин
{
    public partial class FormAdmin : Form
    {
        public FormAdmin()
        {
            InitializeComponent();
        }

        Excel.Application excelApp;     //Все для работы с Excel
        Excel.Workbook excelWorkBook;
        Excel.Worksheet excelWorkSheet;
        Excel.Range excelRange;
        string path = Application.StartupPath;
        string fileName;
        bool saved = true;			//Флажок для необходимости сохранения документа

        private void FormAdmin_FormClosed(object sender, FormClosedEventArgs e) //чтобы приложение закрывалось по крестику
        {
            Application.Exit();                
        }

        private void button_main_Click(object sender, EventArgs e)              //вернуться обратно на главную страницу
        {
            //Удалить все связи с Excel
            if (saved == false)      //Если были внесены изменения, но не сохраняли
            {
                MessageBox.Show("Вы не сохранили изменения в Excel.  \r\n Нажмите кнопку СОХРАНИТЬ");
                  return;
            }

            excelWorkBook.Close(true, null, null);
            excelApp.Quit();
            //Вызвать подпрограмму убивания процессов
            releaseObject(excelWorkSheet);
            releaseObject(excelWorkBook);
            releaseObject(excelApp);

            FormMain FM = new FormMain();
            FM.Location = new Point(this.Left, this.Top);
            FM.Show();
            this.Hide();
        }

        private void FormAdmin_Load(object sender, EventArgs e)
        {
            //Работа с паролем при открытии формы
            string res = Microsoft.VisualBasic.Interaction.InputBox("Введите пароль для доступа к администратору", "Контроль паролем", "Пароль");
            if (res == "Пароль") MessageBox.Show("Вы зашли как Администратор");
            //Открыть Excel и считать из него в компоненты
            excelApp = new Excel.Application();                 //Создать объект Excel
            fileName = path + @"\World of Burgers.xlsx";        //Открыть книгу (файл) Excel
            excelWorkBook = excelApp.Workbooks.Open(fileName);
            //Перенос названий категорий в основной список
            excelWorkSheet = (Excel.Worksheet)excelWorkBook.Worksheets.get_Item("Категории");
            this.listBoxKategory.Items.Clear();
            excelRange = excelWorkSheet.UsedRange;              //Заполненные ячейки 1-ого листа
            for (int row = 1; row <= excelRange.Rows.Count; row++)
            {
                string value = excelRange.Cells[row, 1].value2; //Значение ячейки
                this.listBoxKategory.Items.Add(value);          //Перенос в список
            }
            saved = true;                                       //Изменений пока нет
        }

        private void updataKategory()                                           //метод для обновления категорий
        {
            Excel.Worksheet sheet;      //Лист
            Excel.Range range;          //Ячейки, которые будут подвергаться изменениям
            string value;               //Значение ячейки
            //Ссылка на конкретный лист по названию
            sheet = (Excel.Worksheet)excelWorkBook.Worksheets.get_Item("Категории");
            range = sheet.UsedRange;        //Ссылка на заполненные ячейки листа
            //Сначала очистим заполненные ячейки
            for (int row = 1; row <= range.Rows.Count; row++)
            {
                value = range.Cells[row, 1].value2; //Установиться на нужную ячейку
                range.Replace(value, "");           //Меняем значение в ячейке на пустую строку
            }
            //Заполним новыми значениями из списка категорий
            for (int i = 0; i < listBoxKategory.Items.Count; i++)   //Перебираем все строки списка
            {
                range = (Excel.Range)sheet.Cells[i + 1, 1];         //Первая колонка
                range.Value2 = listBoxKategory.Items[i].ToString(); //Переносим из списка
            }
        }

        private void button_del_cat_Click(object sender, EventArgs e)           //удаление категории
        {
            int sel = this.listBoxKategory.SelectedIndex;   //Номер выделенной строки в списке
            if (sel == -1)          //Если категория для удаления не выбрана 
            {
                MessageBox.Show("Вы не выбрали категорию для удаления!");
                return;
            }

            if (MessageBox.Show("Вы действительно хотите удалить категорию?", "Подтверждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                //Название удаляемой категории
                string kategoriy = this.listBoxKategory.SelectedItem.ToString();
                this.listBoxKategory.Items.RemoveAt(sel);    //Удаляем из списка категорий
                this.listBox_bludo.Items.Clear();            //Очищаем список блюд
                //Обновляем лист "Категории" с учетом удаленной категории
                updataKategory();                            //Вызвали разработанный нами свой метод обновления
                //Удаляем лист с удаляемой категорией из Excel документа
                excelApp.DisplayAlerts = false;              //Не выдавать сообщений пользователю
                excelWorkBook.Worksheets[kategoriy].Delete();//Удаление листа по его названию
                excelApp.DisplayAlerts = true;               //Выдавать сообщения пользователю
                saved = false;                               //Документ не сохранен после изменений
            }
        }

        public void releaseObject(object obj)                                   //метод для уничтожения процессов
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Не могу освободить объект " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void button_save_excel_Click(object sender, EventArgs e)        //сохранить в эксель
        {
            excelWorkBook.Saved = false;
            //Будем спрашивать разрешение на запись поверх существующего документа
            excelApp.DisplayAlerts = true;
            excelWorkBook.Save();       //Сохранение с текущим названием
            saved = true;				//Сохранение выполнено
            updataKategory();
        }

        private void button_add_cat_Click(object sender, EventArgs e)           //добавление категории
        {
            if (textBox_name_cat.Text == "")
            {
                MessageBox.Show("Вы не ввели имя для новой категории!");
                return;
            }

            if (MessageBox.Show("Вы действительно хотите добавить категорию?", "Подтверждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Excel.Worksheet x;
                excelApp.DisplayAlerts = false;    //Не выдавать сообщений пользователю
                //Добавить новый лист и получить ссылку на него
                x = excelWorkBook.Worksheets.Add(After: excelWorkBook.ActiveSheet);

                int index = x.Index;               //получить индекс созданного листа

                excelApp.DisplayAlerts = false;    //Не выдавать сообщений пользователю
                //Получить ссылку на лист с заданным именем
                x = excelWorkBook.Worksheets[index];
                //Присвоить этому листу новое имя
                x.Name = textBox_name_cat.Text;
                excelApp.DisplayAlerts = true;     //Выдавать сообщения пользователю   

                updataKategory();                  //Вызвали разработанный нами свой метод обновления
                saved = false;                     //Документ не сохранен после изменений
            }
        }

        private void button_rename_cat_Click(object sender, EventArgs e)        //переименование категории
        {
            if (textBox_name_cat.Text == "")
            {
                MessageBox.Show("Вы не ввели новое имя для категории!");
                return;
            }

            int sel = this.listBoxKategory.SelectedIndex;   //Номер выделенной строки в списке
            if (sel == -1)                                  //Если категория для удаления не выбрана 
            {
                MessageBox.Show("Вы не выбрали категорию!");
                return;
            }

            if (MessageBox.Show("Вы действительно хотите переименовать категорию?", "Подтверждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Excel.Worksheet x;
                excelApp.DisplayAlerts = false;              //Не выдавать сообщений пользователю

                string oldNameSheet = listBoxKategory.Items[sel].ToString();
                //Получить ссылку на лист с заданным именем
                x = excelWorkBook.Worksheets[oldNameSheet];
                //Присвоить этому листу новое имя
                x.Name = textBox_name_cat.Text;
                excelApp.DisplayAlerts = true;               //Выдавать сообщения пользователю

                updataKategory();                            //Вызвали разработанный нами свой метод обновления
                saved = false;                               //Документ не сохранен после изменений
            }
        }

        private void updataBludo(string nameSheet)                              //метод для обновления блюд
        {
            Excel.Worksheet sheet;      //Лист Excel, с которым надо работать
            Excel.Range range;          //Ячейки листа для редактирования
            string value;
            //Ссылка на конкретный лист по названию – входной параметр
            sheet = (Excel.Worksheet)excelWorkBook.Worksheets.get_Item(nameSheet);
            //Ссылка на заполненные ячейки листа
            range = sheet.UsedRange;
            //Сначала очистим заполненные ячейки по двум колонкам
            for (int row = 1; row <= range.Rows.Count; row++)
            {
                value = range.Cells[row, 1].value2;                     //Значение первой колонки – название блюда
                range.Replace(value, "");                               //Меняем значение в ячейке на пустую строку
                value = Convert.ToString(range.Cells[row, 2].value2);   //Цена блюда
                range.Replace(value, "");                               //Меняем значение в ячейке на пустую строку
            }
            //Заполним новыми значениями из списка: название-цена
            for (int i = 0; i < listBox_bludo.Items.Count; i++)         // бегаем по строкам списка блюд
            {
                string[] temp = listBox_bludo.Items[i].ToString().Split('-'); //Разбиение на 2 части
                range = (Excel.Range)sheet.Cells[i + 1, 1];             //Первый столбец
                range.Value2 = temp[0];                                 //Название блюда из списка
                range = (Excel.Range)sheet.Cells[i + 1, 2];             //Второй столбец
                range.Value2 = temp[1];                                 //Цена блюда
            }
        }

        private void listBoxKategory_Click(object sender, EventArgs e)          //выбор категории
        {          
            if (listBoxKategory.SelectedIndex == -1)
            {
                return;
            }

            //Получить название категории из списка категорий, чтобы связаться с нужным листом
            string category = listBoxKategory.SelectedItem.ToString();
            //Получить доступ к нужному листу по полученной категории
            excelWorkSheet = (Excel.Worksheet)excelWorkBook.Worksheets.get_Item(category);
            //Получить все заполненные ячейки
            excelRange = excelWorkSheet.UsedRange;
            //Перенести два столбца из листа Excel в списки
            listBox_bludo.Items.Clear();
            //Перебрать все заполенные ячейки листа Excel
            for (int row = 1; row <= excelRange.Rows.Count; row++)
            {
                //Название блюда из 1-ой колонки
                string name = excelRange.Cells[row, 1].value2;
                //Цену блюда из 2-ой колонки
                string price = Convert.ToString(excelRange.Cells[row, 2].value2);
                //Соединить название и строку в одну строку
                string bludo = name + " - " + price;
                //Добавить информацию о блюде в список
                this.listBox_bludo.Items.Add(bludo);
            }
        }

        private void button_del_bludo_Click(object sender, EventArgs e)         //удаление блюда
        {
            int selBludo = this.listBox_bludo.SelectedIndex;               //Индекс удаляемого блюда
            if (selBludo == -1)
            {
                MessageBox.Show("Вы не выделили блюдо для удаления!");
                return;
            }
            else
            if (MessageBox.Show("Вы действительно хотите удалить блюдо?", "Подтверждение",
                           MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                string nameSheet = listBoxKategory.SelectedItem.ToString(); //Название листа
                this.listBox_bludo.Items.RemoveAt(selBludo);                //Удалить блюдо из списка
                updataBludo(nameSheet);                                     //Обновить лист категории, из которой удаляется блюдо 
            }

        }

        private void button_add_bludo_Click(object sender, EventArgs e)         //добавление блюда
        {
            if (listBoxKategory.SelectedIndex == -1)
            {
                MessageBox.Show("Вы не выбрали категорию для добавления!");
                return;
            }
            else if (textBox_name_bludo.Text == "" && textBox_price_bludo.Text == "")
            {
                MessageBox.Show("Вы не ввели название и цену блюда!");
                return;
            }
            else if (textBox_name_bludo.Text != "" && textBox_price_bludo.Text == "")
            {
                MessageBox.Show("Вы не ввели цену блюда!");
                return;
            }
            else if (textBox_name_bludo.Text == "" && textBox_price_bludo.Text != "")
            {
                MessageBox.Show("Вы не ввели название блюда!");
                return;
            }

            listBox_bludo.Items.Add(textBox_name_bludo.Text + " - " + textBox_price_bludo.Text);
            updataBludo(listBoxKategory.SelectedItem.ToString());
        }

        private void button_rename_bludo_Click(object sender, EventArgs e)      //изменение блюда
        {
            if (listBoxKategory.SelectedIndex == -1)
            {
                MessageBox.Show("Вы не выбрали категорию для добавления!");
                return;
            }
            else if (listBox_bludo.SelectedIndex == -1)
            {
                MessageBox.Show("Вы не выбрали блюдо для изменения!");
                return;
            }
            else if (textBox_name_bludo.Text == "" && textBox_price_bludo.Text == "")
            {
                MessageBox.Show("Вы не ввели название и цену блюда!");
                return;
            }
            else if (textBox_name_bludo.Text != "" && textBox_price_bludo.Text == "")
            {
                MessageBox.Show("Вы не ввели цену блюда!");
                return;
            }
            else if (textBox_name_bludo.Text == "" && textBox_price_bludo.Text != "")
            {
                MessageBox.Show("Вы не ввели название блюда!");
                return;
            }

            listBox_bludo.Items[listBox_bludo.SelectedIndex] = textBox_name_bludo.Text + " - " + textBox_price_bludo.Text;
            updataBludo(listBoxKategory.SelectedItem.ToString());
        }
    }
}
