﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Электронный_магазин
{
    public partial class FormMenu : Form
    {
        public FormMenu()
        {
            InitializeComponent();
        }

        Excel.Application excelApp;      //Приложение Excel
        Excel.Workbook excelWorkBook;    //Отдельная книга – файл xlc
        Excel.Worksheet excelWorkSheet;  //Один лист
        Excel.Range excelRange;	         //Ячейки
        int sum_zak = 0;                 //сумма заказа
        int purse = 0;                   //сумма на электронном кошельке
        int purse2 = 0;

        private void FormMenu_FormClosed(object sender, FormClosedEventArgs e)  //закрытие приложения
        {
            Application.Exit();                //чтобы приложение закрывалось по крестику


            excelWorkBook.Close(true, null, null);  //Закрыть книгу
            excelApp.Quit();				     	//Разрушить объект Excel
            releaseObject(excelWorkSheet);          //Уничтожить лист
            releaseObject(excelWorkBook);           //Уничтожить книгу
            releaseObject(excelApp);                //Удалить из Диспетчера задач
        } 

        private void button_main_Click(object sender, EventArgs e)              //вернуться на главную
        {
            FormMain FM = new FormMain();                     //открываем главную форму
            FM.Location = new Point(this.Left, this.Top);
            FM.Show();
            this.Hide();
        }

        private void button_bin_Click(object sender, EventArgs e)               //перейти в корзину
        {
            FormBin FB = new FormBin(this.listBox_bluda);                      //открываем форму корзина
            FB.Location = new Point(this.Left, this.Top);
            FB.Show();
            this.Hide();
        }      

        private void FormMenu_Load(object sender, EventArgs e)
        {
            sum_zak = 0;

            Random rand = new Random();
            purse = rand.Next(1000, 5000);
            purse2 = purse;
            progressBar1.Maximum = purse;                             //сумма на кошельке
            progressBar1.Value = purse;
            label_purse.Text = purse + " руб.";

            excelApp = new Excel.Application();                 	  //Создать объект Excel
            //string path = Application.StartupPath;                  //Путь к exe-файлу приложения
            //string fileName = path + @"\World of Burgers.xlsx";     //Абсолютный путь к файлу Excel
            excelWorkBook = excelApp.Workbooks.Open(Application.StartupPath + @"\World of Burgers.xlsx");      //Открыть книгу Excel
            excelWorkSheet = (Excel.Worksheet)excelWorkBook.Worksheets.get_Item("Категории");                  //Доступ к листу
            excelRange = excelWorkSheet.UsedRange;	                  //Заполненные ячейки листа

            comboBox1.Items.Clear();
            int countRows = excelRange.Rows.Count;                    //Количество заполненных строк
            for (int i = 1; i <= countRows; i++)
            {
                string category = excelRange.Cells[i, 1].Value;
                comboBox1.Items.Add(category);
            }
        }

        public void releaseObject(object obj)                                  //метод для уничтожения процессов
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Не могу освободить объект " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string category = comboBox1.Text;                      //Выбранная категория
                                                                   //Связь с листом Excel с названием выбранной категории
            excelWorkSheet = (Excel.Worksheet)excelWorkBook.Worksheets.get_Item(category);
            excelRange = excelWorkSheet.UsedRange;                 //Заполненные ячейки Excel на листе
            checkedListBox_cat.Items.Clear();                         //Подготовить список блюд
            //Считываем все данные из ячеек Excel: название и цена и помещаем в флажок
            for (int row = 1; row <= excelRange.Rows.Count; row++)
            {
                String name = excelRange.Cells[row, 1].value2;     //Название блюда
                String price = Convert.ToString(excelRange.Cells[row, 2].value2); //Цена блюда
                string bludo = name + " - " + price;               //Соединяем все вместе
                this.checkedListBox_cat.Items.Add(bludo);             //Помещаем во флажок
            }
        }

        private void button_zakaz_Click(object sender, EventArgs e)            //перенести в заказ
        {
            string bludo; //Название - цена
            int price; //Только цена
            string[] temp; //Массив разбитых фрагментов: temp[0]-название, temp[1]-цена
            char[] sep = {'-'};
            int summa = 0; //Сумма за эту категорию блюд
            for (int i = 0; i < checkedListBox_cat.Items.Count; i++) //Просмотр всех блюд
            {
                if (checkedListBox_cat.GetItemChecked(i)) //Если блюдо выбрано
                {
                    bludo = this.checkedListBox_cat.Items[i].ToString();
                    temp = bludo.Split(sep, StringSplitOptions.RemoveEmptyEntries); //Разбить на название и цену
                    price = Convert.ToInt32(temp[1]); //Получить цену блюда
                    summa += price; //Учесть в сумме за эти блюда
                    if (purse-(summa+sum_zak)<0)
                    {
                        MessageBox.Show("На вашем счёте недостаточно средств!");
                        return;
                    }
                    else
                        listBox_bluda.Items.Add(bludo); //В список выбранных его добавить
                }
            }
            sum_zak += summa; //Накопить сумму заказа из выбранных блюд категории
            progressBar1.Value = purse - sum_zak;
            label_purse.Text = (purse - sum_zak).ToString() + " руб.";
        }

        private void button_zakaz_delete_Click(object sender, EventArgs e)     //удалить из заказа
        {
            int index = listBox_bluda.SelectedIndex;

            if (index == -1)
            {
                MessageBox.Show("Вы не выбрали блюдо!");
                return;
            }

            string bludo; //Название - цена
            int price; //Только цена
            string[] temp; //Массив разбитых фрагментов: temp[0]-название, temp[1]-цена
            char[] sep = { '-' };

            bludo = listBox_bluda.Items[index].ToString();
            temp = bludo.Split(sep, StringSplitOptions.RemoveEmptyEntries); //Разбить на название и цену
            price = Convert.ToInt32(temp[1]); //Получить цену блюда
            purse = progressBar1.Value;
            purse += price;
            progressBar1.Value = purse;
            label_purse.Text = purse.ToString() + " руб.";

            listBox_bluda.Items.RemoveAt(index);
        }

        private void button_zakaz_clear_Click(object sender, EventArgs e)      //очистить заказ
        {
            listBox_bluda.Items.Clear();
            progressBar1.Value = purse2;
            label_purse.Text = purse2 + " руб.";
            purse = purse2;
        }
    }
}
