﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Электронный_магазин
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        //public static int purse;

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();                //чтобы приложение закрывалось по крестику
        }

        private void button_menu_Click(object sender, EventArgs e)
        {
            FormMenu FM = new FormMenu();
            FM.Location = new Point(this.Left, this.Top);
            FM.Show();
            this.Hide();
        }

        private void button_admin_Click(object sender, EventArgs e)
        {
            FormAdmin FA = new FormAdmin();
            FA.Location = new Point(this.Left, this.Top);
            FA.Show();
            this.Hide();
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            //Random rand = new Random();
            //int purse = rand.Next(1000, 5000);
        }
    }
}
