﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;


namespace Электронный_магазин
{
    public partial class FormBin : Form
    {
        public FormBin(ListBox lb)
        {
            InitializeComponent();
            for (int i = 0; i < lb.Items.Count; i++) //Перенести во внутренней список
                zakaz.Items.Add(lb.Items[i].ToString());
        }


        Word.Application  wordApp;             //Приложение Word
        Word.Document     wordDoc;             //Документ Word
        Word.Table        wordTable;           //Таблица
        Word.InlineShape  wordShape;           //Рисунок
        Word.Paragraph    wordPar, tablePar;   //Абзацы документа и таблицы
        Word.Range        wordRange, tablRange;//Тест абзаца и таблицы


        //Внутренний список для обработки заказанных блюд
        //В него запишется список с формы «Заказ клиента»
        ListBox zakaz = new ListBox();
        public int summa; //Глобальная итоговая сумма – вернется назад

        private void FormBin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();                //чтобы приложение закрывалось по крестику
        }

        private void button_menu_Click(object sender, EventArgs e)           //вернуться в меню
        {
            FormMenu FM = new FormMenu();
            FM.Location = new Point(this.Left, this.Top);
            FM.Show();
            this.Hide();
        }

        private void FormBin_Load(object sender, EventArgs e)
        {
            string t;
            int k = 0, cur = 0, cena;
            summa = 0;
            //Поиск совпадение в названии блюд и удаление совпадающих
            while (zakaz.Items.Count >= 1) //Пока есть элемента в списке
            {
                t = zakaz.Items[0].ToString(); //Текущее блюдо, которое ищем среди всех
                k = 0; //Совпадение пока не найдено этого блюда
                cur = 0; //Позиция поиска совпадение
                while (cur < zakaz.Items.Count) //Пока есть где искать совпадения
                {
                    if (zakaz.Items[cur].ToString() == t) //Совпадение

                    {
                        k++; //Учет этого блюда в количестве
                        zakaz.Items.RemoveAt(cur); //Удаляем повторение, чтобы не учитывать снова
                    }
                    else //Блюда не совпали
                        cur++; //Просматриваем следующее блюда в оставшемся списке
                }
                //Для обработанного блюда помещаем информацию в таблицу
                this.dataGridView_bin.Rows.Add(); //Новая строка в таблице для блюда
                int d = this.dataGridView_bin.RowCount; //Индекс последней строки в таблице
                this.dataGridView_bin.Rows[d - 1].Cells[0].Value = t.Split('-')[0];//Название
                cena = Convert.ToInt32(t.Split('-')[1]); //Получить цену из строки
                this.dataGridView_bin.Rows[d - 1].Cells[1].Value = cena.ToString();
                this.dataGridView_bin.Rows[d - 1].Cells[2].Value = k; //Количество этого блюда
                this.dataGridView_bin.Rows[d - 1].Cells[3].Value = k * cena;//Стоимость
                summa += (k * cena); //Учет в итоговой сумме
            } //Завершение цикла по всем блюдам
            this.textBox_summ.Text = summa.ToString();
        }

        private void button_checkout_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == false && radioButton2.Checked == false)
            {
                MessageBox.Show("Вы не выбрали способ оплаты!");
                return;
            }
            else if (radioButton2.Checked)
            {
                FormCard FC = new FormCard();
                FC.Location = new Point(this.Left, this.Top);
                FC.Show();
                this.Hide();
            }
            else if (radioButton1.Checked)
            {

            }



            Random rand = new Random();
            wordApp = new Word.Application();     //Создание приложения Word
            wordDoc = wordApp.Documents.Add();    //Добавление документа

            //Добавить логитип-картинку
            string path = @"G:\Прочее\Колледж\Проекты С#\Для колледжа\Электронный магазин\Электронный магазин\bin\Debug";
            wordPar = wordDoc.Paragraphs.Add();
            wordShape = wordDoc.InlineShapes.AddPicture(path + @"\Логотип.png", Type.Missing, Type.Missing, wordRange);
            wordShape.Width = 70;
            wordShape.Height = 70;

            //Первый параграф – заголовок документа
            wordPar = wordDoc.Paragraphs.Add();
            wordPar.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
            wordRange = wordPar.Range;
            //wordPar.set_Style("Выделенная цитата");     //Стиль, взятый из Word
            wordRange.Font.Size = 17;
            wordRange.Font.Color = Word.WdColor.wdColorGray95;

            //Текст первого абзаца – заголовка документа
            int n = rand.Next(5000, 10000);
            wordRange.Text = "Номер заказа: WOB" + n + "\t\t\t\tДата: " + DateTime.Now.ToLongDateString();
            wordPar = wordDoc.Paragraphs.Add();

            //Второй параграф - просто текст
            wordPar = wordDoc.Paragraphs.Add();
            wordPar.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
            wordRange = wordPar.Range;
            wordRange.Font.Size = 12;
            wordRange.Font.Color = Word.WdColor.wdColorGray95;
            wordRange.Font.Name = "Arial";
            wordRange.Text = "Список заказанных блюд";

            //Третий параграф - таблица
            tablePar = wordDoc.Paragraphs.Add();
            tablRange = tablePar.Range;

            //Число строк в таблицы совпадает с число строк в таблице заказов формы
            wordTable = wordDoc.Tables.Add(tablRange, this.dataGridView_bin.RowCount + 1, 4);
            wordTable.Borders.InsideLineStyle = wordTable.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleDouble;

            //Заголовков таблицы
            Word.Range cellRange;
            cellRange = wordTable.Cell(1, 1).Range;
            cellRange.Text = "Название блюда";
            cellRange = wordTable.Cell(1, 2).Range;
            cellRange.Text = "Цена блюда";
            cellRange = wordTable.Cell(1, 3).Range;
            cellRange.Text = "Количество";
            cellRange = wordTable.Cell(1, 4).Range;
            cellRange.Text = "Стоимость";
            //Заполнение ячеек таблицы из ячеек таблицы заказов
            //Word.WdParagraphAlignment.wdAlignParagraphCenter;

            for (int i = 2; i <= this.dataGridView_bin.RowCount + 1; i++)
            {
                for (int j = 1; j <= 4; j++)
                {
                    cellRange = wordTable.Cell(i, j).Range;
                    cellRange.Text = this.dataGridView_bin.Rows[i - 2].Cells[j - 1].Value.ToString();
                }
            }

            //Четвертый параграф - итоги
            wordRange.InsertParagraphAfter();
            wordPar = wordDoc.Paragraphs.Add();
            wordPar = wordDoc.Paragraphs.Add();
            wordRange = wordPar.Range;
            wordRange.Font.Color = Word.WdColor.wdColorGray95;
            wordRange.Font.Size = 20;
            wordRange.Text = "Итого:      " + summa.ToString() + "руб.";

            //Сохранение документа
            string fileName = @"/Чек WOB" + n;
            wordDoc.SaveAs(path + fileName + ".docx");

            //Завершение работы с Word
            wordDoc.Close(true, null, null);    //Сначала закрыть документ
            wordApp.Quit();                     //Выход из Word

            //Вызвать свою подпрограмму убивания процессов
            releaseObject(wordPar);      //Уничтожить абзац
            releaseObject(wordDoc);      //Уничтожить документ
            releaseObject(wordApp);      //Удалить из Диспетчера задач


            MessageBox.Show("Ваш заказ готов! Спасибо, что заказываете у нас!");
        }

        public void releaseObject(object obj)  //метод для убивания процессов
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Не могу освободить объект" + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
