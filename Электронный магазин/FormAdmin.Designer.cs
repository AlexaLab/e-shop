﻿namespace Электронный_магазин
{
    partial class FormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_main = new System.Windows.Forms.Button();
            this.listBoxKategory = new System.Windows.Forms.ListBox();
            this.listBox_bludo = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button_del_cat = new System.Windows.Forms.Button();
            this.button_add_cat = new System.Windows.Forms.Button();
            this.button_rename_cat = new System.Windows.Forms.Button();
            this.textBox_name_cat = new System.Windows.Forms.TextBox();
            this.button_save_excel = new System.Windows.Forms.Button();
            this.button_rename_bludo = new System.Windows.Forms.Button();
            this.button_add_bludo = new System.Windows.Forms.Button();
            this.button_del_bludo = new System.Windows.Forms.Button();
            this.textBox_name_bludo = new System.Windows.Forms.TextBox();
            this.textBox_price_bludo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_main
            // 
            this.button_main.BackColor = System.Drawing.Color.White;
            this.button_main.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_main.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_main.Location = new System.Drawing.Point(397, 572);
            this.button_main.Name = "button_main";
            this.button_main.Size = new System.Drawing.Size(350, 52);
            this.button_main.TabIndex = 3;
            this.button_main.Text = "Перейти на главную страницу";
            this.button_main.UseVisualStyleBackColor = false;
            this.button_main.Click += new System.EventHandler(this.button_main_Click);
            // 
            // listBoxKategory
            // 
            this.listBoxKategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxKategory.FormattingEnabled = true;
            this.listBoxKategory.ItemHeight = 16;
            this.listBoxKategory.Location = new System.Drawing.Point(14, 60);
            this.listBoxKategory.Name = "listBoxKategory";
            this.listBoxKategory.Size = new System.Drawing.Size(350, 292);
            this.listBoxKategory.TabIndex = 4;
            this.listBoxKategory.Click += new System.EventHandler(this.listBoxKategory_Click);
            // 
            // listBox_bludo
            // 
            this.listBox_bludo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox_bludo.FormattingEnabled = true;
            this.listBox_bludo.ItemHeight = 16;
            this.listBox_bludo.Location = new System.Drawing.Point(397, 60);
            this.listBox_bludo.Name = "listBox_bludo";
            this.listBox_bludo.Size = new System.Drawing.Size(350, 292);
            this.listBox_bludo.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Bahnschrift Condensed", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 33);
            this.label2.TabIndex = 6;
            this.label2.Text = "Категории";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Condensed", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(391, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 33);
            this.label1.TabIndex = 7;
            this.label1.Text = "Блюда в категории";
            // 
            // button_del_cat
            // 
            this.button_del_cat.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_del_cat.BackColor = System.Drawing.Color.White;
            this.button_del_cat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_del_cat.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_del_cat.Location = new System.Drawing.Point(14, 358);
            this.button_del_cat.Name = "button_del_cat";
            this.button_del_cat.Size = new System.Drawing.Size(350, 40);
            this.button_del_cat.TabIndex = 15;
            this.button_del_cat.Text = "Удалить категорию";
            this.button_del_cat.UseVisualStyleBackColor = false;
            this.button_del_cat.Click += new System.EventHandler(this.button_del_cat_Click);
            // 
            // button_add_cat
            // 
            this.button_add_cat.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_add_cat.BackColor = System.Drawing.Color.White;
            this.button_add_cat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_add_cat.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_add_cat.Location = new System.Drawing.Point(14, 404);
            this.button_add_cat.Name = "button_add_cat";
            this.button_add_cat.Size = new System.Drawing.Size(350, 40);
            this.button_add_cat.TabIndex = 16;
            this.button_add_cat.Text = "Добавить категорию";
            this.button_add_cat.UseVisualStyleBackColor = false;
            this.button_add_cat.Click += new System.EventHandler(this.button_add_cat_Click);
            // 
            // button_rename_cat
            // 
            this.button_rename_cat.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_rename_cat.BackColor = System.Drawing.Color.White;
            this.button_rename_cat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_rename_cat.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_rename_cat.Location = new System.Drawing.Point(14, 450);
            this.button_rename_cat.Name = "button_rename_cat";
            this.button_rename_cat.Size = new System.Drawing.Size(350, 40);
            this.button_rename_cat.TabIndex = 17;
            this.button_rename_cat.Text = "Переименовать категорию";
            this.button_rename_cat.UseVisualStyleBackColor = false;
            this.button_rename_cat.Click += new System.EventHandler(this.button_rename_cat_Click);
            // 
            // textBox_name_cat
            // 
            this.textBox_name_cat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_name_cat.Location = new System.Drawing.Point(14, 531);
            this.textBox_name_cat.Name = "textBox_name_cat";
            this.textBox_name_cat.Size = new System.Drawing.Size(350, 23);
            this.textBox_name_cat.TabIndex = 18;
            // 
            // button_save_excel
            // 
            this.button_save_excel.BackColor = System.Drawing.Color.White;
            this.button_save_excel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_save_excel.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_save_excel.Location = new System.Drawing.Point(14, 572);
            this.button_save_excel.Name = "button_save_excel";
            this.button_save_excel.Size = new System.Drawing.Size(350, 52);
            this.button_save_excel.TabIndex = 19;
            this.button_save_excel.Text = "Сохранить в Excel";
            this.button_save_excel.UseVisualStyleBackColor = false;
            this.button_save_excel.Click += new System.EventHandler(this.button_save_excel_Click);
            // 
            // button_rename_bludo
            // 
            this.button_rename_bludo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_rename_bludo.BackColor = System.Drawing.Color.White;
            this.button_rename_bludo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_rename_bludo.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_rename_bludo.Location = new System.Drawing.Point(397, 450);
            this.button_rename_bludo.Name = "button_rename_bludo";
            this.button_rename_bludo.Size = new System.Drawing.Size(350, 40);
            this.button_rename_bludo.TabIndex = 22;
            this.button_rename_bludo.Text = "Изменить блюдо";
            this.button_rename_bludo.UseVisualStyleBackColor = false;
            this.button_rename_bludo.Click += new System.EventHandler(this.button_rename_bludo_Click);
            // 
            // button_add_bludo
            // 
            this.button_add_bludo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_add_bludo.BackColor = System.Drawing.Color.White;
            this.button_add_bludo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_add_bludo.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_add_bludo.Location = new System.Drawing.Point(397, 404);
            this.button_add_bludo.Name = "button_add_bludo";
            this.button_add_bludo.Size = new System.Drawing.Size(350, 40);
            this.button_add_bludo.TabIndex = 21;
            this.button_add_bludo.Text = "Добавить блюдо";
            this.button_add_bludo.UseVisualStyleBackColor = false;
            this.button_add_bludo.Click += new System.EventHandler(this.button_add_bludo_Click);
            // 
            // button_del_bludo
            // 
            this.button_del_bludo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_del_bludo.BackColor = System.Drawing.Color.White;
            this.button_del_bludo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_del_bludo.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_del_bludo.Location = new System.Drawing.Point(397, 358);
            this.button_del_bludo.Name = "button_del_bludo";
            this.button_del_bludo.Size = new System.Drawing.Size(350, 40);
            this.button_del_bludo.TabIndex = 20;
            this.button_del_bludo.Text = "Удалить блюдо";
            this.button_del_bludo.UseVisualStyleBackColor = false;
            this.button_del_bludo.Click += new System.EventHandler(this.button_del_bludo_Click);
            // 
            // textBox_name_bludo
            // 
            this.textBox_name_bludo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_name_bludo.Location = new System.Drawing.Point(397, 531);
            this.textBox_name_bludo.Name = "textBox_name_bludo";
            this.textBox_name_bludo.Size = new System.Drawing.Size(252, 23);
            this.textBox_name_bludo.TabIndex = 23;
            // 
            // textBox_price_bludo
            // 
            this.textBox_price_bludo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_price_bludo.Location = new System.Drawing.Point(668, 531);
            this.textBox_price_bludo.Name = "textBox_price_bludo";
            this.textBox_price_bludo.Size = new System.Drawing.Size(79, 23);
            this.textBox_price_bludo.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(12, 504);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 24);
            this.label3.TabIndex = 25;
            this.label3.Text = "категория:";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(393, 504);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 24);
            this.label4.TabIndex = 26;
            this.label4.Text = "блюдо:";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(664, 504);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 24);
            this.label5.TabIndex = 27;
            this.label5.Text = "цена:";
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Электронный_магазин.Properties.Resources.Fotolia_5264310_Subscription_XXL;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(759, 636);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_price_bludo);
            this.Controls.Add(this.textBox_name_bludo);
            this.Controls.Add(this.button_rename_bludo);
            this.Controls.Add(this.button_add_bludo);
            this.Controls.Add(this.button_del_bludo);
            this.Controls.Add(this.button_save_excel);
            this.Controls.Add(this.textBox_name_cat);
            this.Controls.Add(this.button_rename_cat);
            this.Controls.Add(this.button_add_cat);
            this.Controls.Add(this.button_del_cat);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBox_bludo);
            this.Controls.Add(this.listBoxKategory);
            this.Controls.Add(this.button_main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Администратор";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormAdmin_FormClosed);
            this.Load += new System.EventHandler(this.FormAdmin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_main;
        private System.Windows.Forms.ListBox listBoxKategory;
        private System.Windows.Forms.ListBox listBox_bludo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_del_cat;
        private System.Windows.Forms.Button button_add_cat;
        private System.Windows.Forms.Button button_rename_cat;
        private System.Windows.Forms.TextBox textBox_name_cat;
        private System.Windows.Forms.Button button_save_excel;
        private System.Windows.Forms.Button button_rename_bludo;
        private System.Windows.Forms.Button button_add_bludo;
        private System.Windows.Forms.Button button_del_bludo;
        private System.Windows.Forms.TextBox textBox_name_bludo;
        private System.Windows.Forms.TextBox textBox_price_bludo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}