﻿namespace Электронный_магазин
{
    partial class FormCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.maskedTextBox_number = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.maskedTextBox_date = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.maskedTextBox_code = new System.Windows.Forms.MaskedTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button_checkout = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Bahnschrift Condensed", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(14, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(252, 33);
            this.label2.TabIndex = 4;
            this.label2.Text = "Оплата банковской картой";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Condensed", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(30, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "Введите номер карты:";
            // 
            // maskedTextBox_number
            // 
            this.maskedTextBox_number.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.maskedTextBox_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBox_number.Location = new System.Drawing.Point(35, 153);
            this.maskedTextBox_number.Mask = "0000-0000-0000-0000";
            this.maskedTextBox_number.Name = "maskedTextBox_number";
            this.maskedTextBox_number.Size = new System.Drawing.Size(201, 27);
            this.maskedTextBox_number.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Bahnschrift Condensed", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(30, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(255, 29);
            this.label3.TabIndex = 7;
            this.label3.Text = "Введите срок действия карты:\r\n";
            // 
            // maskedTextBox_date
            // 
            this.maskedTextBox_date.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.maskedTextBox_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBox_date.Location = new System.Drawing.Point(35, 259);
            this.maskedTextBox_date.Mask = "00/00";
            this.maskedTextBox_date.Name = "maskedTextBox_date";
            this.maskedTextBox_date.Size = new System.Drawing.Size(51, 27);
            this.maskedTextBox_date.TabIndex = 8;
            this.maskedTextBox_date.ValidatingType = typeof(System.DateTime);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Bahnschrift Condensed", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(309, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(227, 29);
            this.label4.TabIndex = 9;
            this.label4.Text = "Введите специальный код:\r\n";
            // 
            // maskedTextBox_code
            // 
            this.maskedTextBox_code.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.maskedTextBox_code.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBox_code.Location = new System.Drawing.Point(314, 259);
            this.maskedTextBox_code.Mask = "000";
            this.maskedTextBox_code.Name = "maskedTextBox_code";
            this.maskedTextBox_code.Size = new System.Drawing.Size(50, 27);
            this.maskedTextBox_code.TabIndex = 10;
            this.maskedTextBox_code.ValidatingType = typeof(int);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Электронный_магазин.Properties.Resources.burger6;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(301, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(222, 181);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // button_checkout
            // 
            this.button_checkout.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_checkout.BackColor = System.Drawing.Color.White;
            this.button_checkout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_checkout.Font = new System.Drawing.Font("Bahnschrift Condensed", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_checkout.Location = new System.Drawing.Point(20, 320);
            this.button_checkout.Name = "button_checkout";
            this.button_checkout.Size = new System.Drawing.Size(516, 52);
            this.button_checkout.TabIndex = 12;
            this.button_checkout.Text = "Оформить заказ";
            this.button_checkout.UseVisualStyleBackColor = false;
            this.button_checkout.Click += new System.EventHandler(this.button_checkout_Click);
            // 
            // FormCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Электронный_магазин.Properties.Resources.Fotolia_5264310_Subscription_XXL;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(553, 399);
            this.Controls.Add(this.button_checkout);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.maskedTextBox_code);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.maskedTextBox_date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.maskedTextBox_number);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Name = "FormCard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Оплата банковской картой";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormCard_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox_number;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox maskedTextBox_date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox maskedTextBox_code;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button_checkout;
    }
}