﻿namespace Электронный_магазин
{
    partial class FormMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_bin = new System.Windows.Forms.Button();
            this.button_main = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label_purse = new System.Windows.Forms.Label();
            this.checkedListBox_cat = new System.Windows.Forms.CheckedListBox();
            this.listBox_bluda = new System.Windows.Forms.ListBox();
            this.button_zakaz = new System.Windows.Forms.Button();
            this.button_zakaz_clear = new System.Windows.Forms.Button();
            this.button_zakaz_delete = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_bin
            // 
            this.button_bin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_bin.BackColor = System.Drawing.Color.White;
            this.button_bin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_bin.Font = new System.Drawing.Font("Bahnschrift Condensed", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_bin.Location = new System.Drawing.Point(340, 572);
            this.button_bin.Name = "button_bin";
            this.button_bin.Size = new System.Drawing.Size(302, 52);
            this.button_bin.TabIndex = 1;
            this.button_bin.Text = "Корзина";
            this.button_bin.UseVisualStyleBackColor = false;
            this.button_bin.Click += new System.EventHandler(this.button_bin_Click);
            // 
            // button_main
            // 
            this.button_main.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_main.BackColor = System.Drawing.Color.White;
            this.button_main.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_main.Font = new System.Drawing.Font("Bahnschrift Condensed", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_main.Location = new System.Drawing.Point(12, 572);
            this.button_main.Name = "button_main";
            this.button_main.Size = new System.Drawing.Size(302, 52);
            this.button_main.TabIndex = 2;
            this.button_main.Text = "На главную страницу";
            this.button_main.UseVisualStyleBackColor = false;
            this.button_main.Click += new System.EventHandler(this.button_main_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 40);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(178, 26);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.Text = "Категории товара";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Bahnschrift Condensed", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(276, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 48);
            this.label2.TabIndex = 4;
            this.label2.Text = "Меню";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(262, 533);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Сумма на вашем кошельке:";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressBar1.Location = new System.Drawing.Point(460, 533);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(114, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 6;
            this.progressBar1.Value = 50;
            // 
            // label_purse
            // 
            this.label_purse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_purse.AutoSize = true;
            this.label_purse.BackColor = System.Drawing.Color.Transparent;
            this.label_purse.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_purse.ForeColor = System.Drawing.Color.White;
            this.label_purse.Location = new System.Drawing.Point(567, 533);
            this.label_purse.Name = "label_purse";
            this.label_purse.Size = new System.Drawing.Size(70, 24);
            this.label_purse.TabIndex = 7;
            this.label_purse.Text = "1000 руб.";
            // 
            // checkedListBox_cat
            // 
            this.checkedListBox_cat.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkedListBox_cat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkedListBox_cat.FormattingEnabled = true;
            this.checkedListBox_cat.Location = new System.Drawing.Point(12, 80);
            this.checkedListBox_cat.Name = "checkedListBox_cat";
            this.checkedListBox_cat.Size = new System.Drawing.Size(302, 378);
            this.checkedListBox_cat.TabIndex = 9;
            // 
            // listBox_bluda
            // 
            this.listBox_bluda.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listBox_bluda.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox_bluda.FormattingEnabled = true;
            this.listBox_bluda.ItemHeight = 16;
            this.listBox_bluda.Location = new System.Drawing.Point(340, 80);
            this.listBox_bluda.Name = "listBox_bluda";
            this.listBox_bluda.Size = new System.Drawing.Size(302, 340);
            this.listBox_bluda.TabIndex = 10;
            // 
            // button_zakaz
            // 
            this.button_zakaz.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_zakaz.BackColor = System.Drawing.Color.White;
            this.button_zakaz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_zakaz.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_zakaz.Location = new System.Drawing.Point(12, 480);
            this.button_zakaz.Name = "button_zakaz";
            this.button_zakaz.Size = new System.Drawing.Size(302, 40);
            this.button_zakaz.TabIndex = 11;
            this.button_zakaz.Text = "Перенести в заказ";
            this.button_zakaz.UseVisualStyleBackColor = false;
            this.button_zakaz.Click += new System.EventHandler(this.button_zakaz_Click);
            // 
            // button_zakaz_clear
            // 
            this.button_zakaz_clear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_zakaz_clear.BackColor = System.Drawing.Color.White;
            this.button_zakaz_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_zakaz_clear.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_zakaz_clear.Location = new System.Drawing.Point(340, 480);
            this.button_zakaz_clear.Name = "button_zakaz_clear";
            this.button_zakaz_clear.Size = new System.Drawing.Size(302, 40);
            this.button_zakaz_clear.TabIndex = 12;
            this.button_zakaz_clear.Text = "Очистить заказ";
            this.button_zakaz_clear.UseVisualStyleBackColor = false;
            this.button_zakaz_clear.Click += new System.EventHandler(this.button_zakaz_clear_Click);
            // 
            // button_zakaz_delete
            // 
            this.button_zakaz_delete.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_zakaz_delete.BackColor = System.Drawing.Color.White;
            this.button_zakaz_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_zakaz_delete.Font = new System.Drawing.Font("Bahnschrift Condensed", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_zakaz_delete.Location = new System.Drawing.Point(340, 434);
            this.button_zakaz_delete.Name = "button_zakaz_delete";
            this.button_zakaz_delete.Size = new System.Drawing.Size(302, 40);
            this.button_zakaz_delete.TabIndex = 13;
            this.button_zakaz_delete.Text = "Удалить из заказа";
            this.button_zakaz_delete.UseVisualStyleBackColor = false;
            this.button_zakaz_delete.Click += new System.EventHandler(this.button_zakaz_delete_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Электронный_магазин.Properties.Resources.burger6;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(583, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(59, 49);
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // FormMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Электронный_магазин.Properties.Resources.Fotolia_5264310_Subscription_XXL;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(654, 636);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button_zakaz_delete);
            this.Controls.Add(this.button_zakaz_clear);
            this.Controls.Add(this.button_zakaz);
            this.Controls.Add(this.listBox_bluda);
            this.Controls.Add(this.checkedListBox_cat);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label_purse);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_main);
            this.Controls.Add(this.button_bin);
            this.Name = "FormMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Меню";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMenu_FormClosed);
            this.Load += new System.EventHandler(this.FormMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_bin;
        private System.Windows.Forms.Button button_main;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label_purse;
        private System.Windows.Forms.CheckedListBox checkedListBox_cat;
        private System.Windows.Forms.ListBox listBox_bluda;
        private System.Windows.Forms.Button button_zakaz;
        private System.Windows.Forms.Button button_zakaz_clear;
        private System.Windows.Forms.Button button_zakaz_delete;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}